import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'main.dart';

class HomePageWidget extends StatefulWidget {
  static const routeName = '/HomePageWidget';
  HomePageWidget({Key? key}) : super(key: key);

  @override
  _HomePageWidgetState createState() => _HomePageWidgetState();
}

class _HomePageWidgetState extends State<HomePageWidget> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  int _stackToView = 1;

  void _handleLoad(String value) {
    setState(() {
      _stackToView = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          key: scaffoldKey,
          body: IndexedStack(
            index: _stackToView,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      flex: 2,
                      child: WebView(
                        initialUrl: "https://drishtientertainers.com/",
                        javascriptMode: JavascriptMode.unrestricted,
                        onPageFinished: _handleLoad,
                        onWebViewCreated:
                            (WebViewController webViewController) {
                          _controller.complete(webViewController);
                        },
                        navigationDelegate: (NavigationRequest nav) {
                          print(
                              'SAW REQ TO LOAD: ${nav.url}\nIN MAIN FRAME? ${nav.isForMainFrame}');
                          return NavigationDecision.navigate;
                        },
                      )),
                ],
              ),
              Container(
                  color: darkColor,
                  child: Center(
                    child: CircularProgressIndicator(
                      color: Colors.white,
                    ),
                  )),
            ],
          )),
    );
  }
}
